//
//  ViewController.swift
//  AutoLayoutByCode
//
//  Created by Admin on 4/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let logoApp = UIImageView()
    let viewContainTextField = UIView()
    let userTextField = UITextField()
    let rememberIDSwitch = UISwitch()
    let rememberIDLabel = UILabel()
    let signupLabel = UILabel()
    let signInButton = UIButton()
    let signUpButton = UIButton()
    var k_height: CGFloat = 0
    var k_width: CGFloat = 0
    var k : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFactor()
        customizeUI()
    }
    
    override func viewDidLayoutSubviews() {
        
    }

    func customizeUI(){
        customizeLogo()
        customizeUserTextField()
        customizeSwitch()
        customizeLabel()
        customizeButton()
    }
    
    func setupFactor(){
        let heightInIphone8: CGFloat = 667
        let widthInIphone8: CGFloat = 375
        let sizeScreen = UIScreen.main.bounds
        k_height = sizeScreen.size.height/heightInIphone8
        k_width = sizeScreen.size.width/widthInIphone8
        k = k_height < k_width ? k_height : k_width
    }
    
    func customizeLogo(){
        logoApp.image = UIImage(named: "biIdallSigninH")
        view.addSubview(logoApp)
        
        logoApp.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = NSLayoutConstraint(item: logoApp, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 92 * k_height)
        let widthConstraint = NSLayoutConstraint(item: logoApp, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 160 * k)
        let heightConstraint = NSLayoutConstraint(item: logoApp, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 70 * k)
        let honizontalConstraint = NSLayoutConstraint(item: logoApp, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        view.addConstraints([topConstraint, widthConstraint, heightConstraint, honizontalConstraint])
    }
    
    func customizeUserTextField(){
        //customize view contain textfield
        viewContainTextField.layer.borderWidth = 1
        viewContainTextField.layer.borderColor = UIColor(red: 126/255, green: 137/255, blue: 157/255, alpha: 1).cgColor
        viewContainTextField.layer.cornerRadius = 45 * k / 9
        view.addSubview(viewContainTextField)
        
        //set constraint for view contain textfield
        viewContainTextField.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint1 = NSLayoutConstraint(item: viewContainTextField, attribute: .top, relatedBy: .equal, toItem: logoApp, attribute: .bottom, multiplier: 1, constant: 72 * k_height)
        let leftConstraint1 = NSLayoutConstraint(item: viewContainTextField, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 43 * k)
        let heightConstraint1 = NSLayoutConstraint(item: viewContainTextField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45 * k)
        let honizontalConstraint1 = NSLayoutConstraint(item: viewContainTextField, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        view.addConstraints([topConstraint1, leftConstraint1, heightConstraint1, honizontalConstraint1])

        //set constraint for user textfield
        viewContainTextField.addSubview(userTextField)
        userTextField.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint2 = NSLayoutConstraint(item: userTextField, attribute: .leading, relatedBy: .equal, toItem: viewContainTextField, attribute: .leading, multiplier: 1, constant: 12)
        let honizontalConstraint2 = NSLayoutConstraint(item: userTextField, attribute: .centerX, relatedBy: .equal, toItem: viewContainTextField, attribute: .centerX, multiplier: 1, constant: 0)
        let verticalConstraint2 = NSLayoutConstraint(item: userTextField, attribute: .centerY, relatedBy: .equal, toItem: viewContainTextField, attribute: .centerY, multiplier: 1, constant: 0)
        viewContainTextField.addConstraints([leftConstraint2, honizontalConstraint2, verticalConstraint2])
    }
    
    func customizeLabel(){
        //customize rememberID
        rememberIDLabel.font = UIFont.systemFont(ofSize: 14 * k)
        rememberIDLabel.textColor = UIColor(red: 3/255, green: 25/255, blue: 64/255, alpha: 0.8)
        rememberIDLabel.text = "Remember ID"
        view.addSubview(rememberIDLabel)
        
        //set constraint for rememberID label
        rememberIDLabel.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint1 = NSLayoutConstraint(item: rememberIDLabel, attribute: .top, relatedBy: .equal, toItem: rememberIDSwitch, attribute: .top, multiplier: 1, constant: 0)
        let heightConstraint1 = NSLayoutConstraint(item: rememberIDLabel, attribute: .height, relatedBy: .equal, toItem: rememberIDSwitch, attribute: .height, multiplier: 1, constant: 0)
        let rightConstraint1 = NSLayoutConstraint(item: rememberIDLabel, attribute: .right, relatedBy: .equal, toItem: rememberIDSwitch, attribute: .left, multiplier: 1, constant: -10 - (47 * (k - 1))/2)
        view.addConstraints([topConstraint1, heightConstraint1, rightConstraint1])
        
        //customize sign up label
        signupLabel.font = UIFont.systemFont(ofSize: 14 * k)
        signupLabel.textColor = UIColor(red: 3/255, green: 25/255, blue: 64/255, alpha: 0.8)
        signupLabel.text = "If you don’t have IDall ID, sign up first"
        signupLabel.textAlignment = .center
        view.addSubview(signupLabel)
    }
    
    func customizeButton(){
        //customize button sign in
        let layer = CAGradientLayer()
        let color1 = UIColor(red: 88/255, green: 222/255, blue: 252/255, alpha: 1).cgColor
        let color2 = UIColor(red: 92/255, green: 130/255, blue: 253/255, alpha: 1).cgColor
        let color3 = UIColor(red: 118/255, green: 51/255, blue: 253/255, alpha: 1).cgColor
        
        layer.frame = CGRect(x: 0, y: 0, width: 1000, height: 100) //button.bounds
        layer.colors = [color1, color2, color3]
        layer.startPoint = CGPoint(x: 0, y: 0)
        layer.endPoint = CGPoint(x: 1, y: 0)
        layer.locations = [0.0, 0.5, 1.0]
        
        signInButton.layer.cornerRadius = 45 * k_height / 2
        signInButton.layer.insertSublayer(layer, at: 0)
        signInButton.titleLabel?.font = UIFont.systemFont(ofSize: 17 * k_height)
        signInButton.setTitleColor(.white, for: .normal)
        signInButton.setTitle("Sign in", for: .normal)
        signInButton.clipsToBounds = true
        view.addSubview(signInButton)
        
        //set constraint button sign in
        signInButton.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint1 = NSLayoutConstraint(item: signInButton, attribute: .top, relatedBy: .equal, toItem: rememberIDSwitch, attribute: .bottom, multiplier: 1, constant: 37 * k_height)
        let heightConstraint1 = NSLayoutConstraint(item: signInButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45 * k_height)
        let leftConstraint1 = NSLayoutConstraint(item: signInButton, attribute: .leading, relatedBy: .equal, toItem: viewContainTextField, attribute: .leading, multiplier: 1, constant: 0)
        let honizontalConstraint1 = NSLayoutConstraint(item: signInButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        view.addConstraints([topConstraint1, heightConstraint1, leftConstraint1, honizontalConstraint1])
        
        //set constraint for sign up label
        signupLabel.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint2 = NSLayoutConstraint(item: signupLabel, attribute: .top, relatedBy: .equal, toItem: signInButton, attribute: .bottom, multiplier: 1, constant: 13 * k_height)
        let heightConstraint2 = NSLayoutConstraint(item: signupLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 16 * k_height)
        let leftConstraint2 = NSLayoutConstraint(item: signupLabel, attribute: .leading, relatedBy: .equal, toItem: signInButton, attribute: .leading, multiplier: 1, constant: 0)
        let honizontalConstraint2 = NSLayoutConstraint(item: signupLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        view.addConstraints([topConstraint2, heightConstraint2, leftConstraint2, honizontalConstraint2])
        
        //customize button sign up
        signUpButton.layer.cornerRadius = 45 * k_height / 2
        signUpButton.layer.borderColor = UIColor(red: 3/255, green: 25/255, blue: 64/255, alpha: 1).cgColor
        signUpButton.layer.borderWidth = 2
        signUpButton.titleLabel?.font = UIFont.systemFont(ofSize: 17 * k_height)
        signUpButton.setTitleColor(UIColor(red: 3/255, green: 25/255, blue: 64/255, alpha: 1), for: .normal)
        signUpButton.setTitle("Sign up", for: .normal)
        view.addSubview(signUpButton)
        
        //set constraint button sign up
        signUpButton.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint3 = NSLayoutConstraint(item: signUpButton, attribute: .top, relatedBy: .equal, toItem: signupLabel, attribute: .bottom, multiplier: 1, constant: 13 * k_height)
        let heightConstraint3 = NSLayoutConstraint(item: signUpButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45 * k_height)
        let leftConstraint3 = NSLayoutConstraint(item: signUpButton, attribute: .leading, relatedBy: .equal, toItem: viewContainTextField, attribute: .leading, multiplier: 1, constant: 0)
        let honizontalConstraint3 = NSLayoutConstraint(item: signUpButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        view.addConstraints([topConstraint3, heightConstraint3, leftConstraint3, honizontalConstraint3])
    }
    
    func customizeSwitch(){
        //customize sw
        rememberIDSwitch.frame.origin = CGPoint(x: 283, y: 286.5)
        rememberIDSwitch.transform = CGAffineTransform(scaleX: k, y: k)
        view.addSubview(rememberIDSwitch)
        
        //set constraint for switch
        rememberIDSwitch.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = NSLayoutConstraint(item: rememberIDSwitch, attribute: .top, relatedBy: .equal, toItem: viewContainTextField, attribute: .bottom, multiplier: 1, constant: 7.5 * k_height * k)
        let rightConstraint = NSLayoutConstraint(item: rememberIDSwitch, attribute: .trailing, relatedBy: .equal, toItem: viewContainTextField, attribute: .trailing, multiplier: 1, constant: -(47 * (k - 1))/2)
        view.addConstraints([topConstraint, rightConstraint])
    }
}

